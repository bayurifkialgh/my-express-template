const BaseController = require('./BaseController') 

class ExampleController extends BaseController {
	index(req, res) {
		return super.successResponse(res, 'Success!', {
			message: 'Keren'
		})
	}
}

module.exports = new ExampleController()
