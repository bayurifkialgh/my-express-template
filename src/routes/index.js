const express = require('express');
const router = express.Router();

const ExampleController = require('../controller/ExampleController');

router.get('/', ExampleController.index);

module.exports = router;
